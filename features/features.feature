Feature: login page

    Scenario: user logins using unregistered data
        Given login page is displayed
        When user enters unregistered username
        And user enters unregistered password
        And user clicks login button
        Then error message is shown

    Scenario: user logins using registered data and tries to logout
        Given login page is displayed (2)
        When user enters registered username
        And user enters registered password
        And user clicks login button (2)
        Then user proceeds to the dashboard page
        When user clicks profile
        And user selects logout
        And user selects yes
        Then user goes back to login page
    
    Scenario: user clicks on forgot password using unregistered email
        Given login page is displayed (3)
        When user clicks forgot password
        And user enters unregistered email
        And user clicks submit
        Then error message "email is not registered yet" is shown
