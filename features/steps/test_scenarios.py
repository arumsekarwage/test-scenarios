from selenium import webdriver
from behave import Given, When, Then
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

@Given('login page is displayed')
def step_impl(context):
    context.browser.get('https://console.awan.sh/')
    context.browser.refresh()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@id=\'root\']/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div/div[2]/input")))

@When('user enters unregistered username')
def step_impl(context):
   context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input').send_keys('aldebaran123')

@When('user enters unregistered password')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/input').send_keys('awanio123')

@When('user clicks login button')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div').click()
    time.sleep(20)

@Then('error message is shown')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[2]/div/div[1]/div[1]')))




@Given('login page is displayed (2)')
def step_impl(context):
    context.browser.get('https://console.awan.sh/')
    context.browser.refresh()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@id=\'root\']/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div/div[2]/input")))

@When('user enters registered username')
def step_impl(context):
   context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input').send_keys('aldebaran')

@When('user enters registered password')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/input').send_keys('awanio')

@When('user clicks login button (2)')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div').click()
    time.sleep(15)

@Then('user proceeds to the dashboard page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div[1]/div/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/div/div[1]/div[2]/div[2]/div[1]')))

@When('user clicks profile')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div[1]/div/div/div/div/div[1]/div/div/div[2]/div/div/div[1]/div/div/div/div[2]/div').click()
    time.sleep(2)

@When('user selects logout')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div[5]/div[2]/div/div/div[4]/div').click()
    time.sleep(3)

@When('user selects yes')
def step_impl(context):
    WebDriverWait(context.browser, 15).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div[6]/div/div[2]/div/div/div[3]/div[2]/div/div/div'))).click()

@Then('user goes back to login page')
def step_impl(context):
    time.sleep(5)
    try:
        WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[1]/div[2]/div/div/div[1]')))
    except NoSuchElementException:
        return False
    return True


@Given('login page is displayed (3)')
def step_impl(context):
    context.browser.get('https://console.awan.sh/')
    context.browser.refresh()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@id=\'root\']/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div/div[2]/div/div[2]/input")))

@When('user clicks forgot password')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[1]/div/div/div').click()
    time.sleep(10)    

@When('user enters unregistered email')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/input'))).send_keys('aldebaran123@gmail.com')
    
@When('user clicks submit')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div[2]/div[2]/div/div/div/div/div').click()
    time.sleep(15)

@Then('error message "email is not registered yet" is shown')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[2]/div/div[1]/div[2]')))