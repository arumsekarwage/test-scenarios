from behave import fixture, use_fixture
from selenium import webdriver
 
@fixture
def launch_browser(context):
    #Launch Browser
    context.browser = webdriver.Chrome("C:\Windows\chromedriver.exe")
    yield context.browser
 
    #Clean Up Browser
    context.browser.quit()
    print("=============>Browser quits")

def before_scenario(context, scenario):
    the_fixture1 = use_fixture(launch_browser, context)